#include <LiquidCrystal.h>

#define LANES 8
#define DEBOUNCE_DELAY 50

#define D0 0
#define D1 1
#define D2 2
#define D3 3
#define D4 4
#define D5 5
#define D6 6
#define D7 7
#define D8 8
#define D9 9
#define D10 10
#define D11 11
#define D12 12
#define D13 13
#define LED D13
#define RESET D12
#define MAX_TIMES 16

const int rs = 11, en = 10, d4 = A5, d5 = A4, d6 = A3, d7 = A2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
const char * sixteenSpaces = "                ";
char buf[17] = {0};

unsigned int IN[LANES] = { D2, D3, D4, D5, D6, D7, D8, D9 };
int previousStates[LANES];
unsigned char presses[LANES];
unsigned long times[LANES][MAX_TIMES]; // Record the last MAX_TIMES presses
unsigned long debounceData[LANES];

int resetState;
unsigned long resetDebounce;

bool timing = false;

void setup() {
  lcd.begin(16, 2);
  printLCD("Initializing");
  Serial.begin(9600);
  for (int i = 0; i < LANES; i++) {
    pinMode(IN[i], INPUT);
  }
  pinMode(RESET, INPUT);
  pinMode(LED, OUTPUT);
  resetData();
}

void flashLED(int count) {
  for (int i = 0; i < count; i++) {
    digitalWrite(LED, HIGH);
    delay(75);
    digitalWrite(LED, LOW);
    delay(75);
  }
}

void printLCD(const char * text) {
  clearLCD();
  lcd.print(text);
}

void printLCD1(const char * text) {
  lcd.setCursor(0, 0);
  lcd.print(sixteenSpaces);
  lcd.setCursor(0, 0);
  lcd.print(text);
}

void printLCD2(const char * text) {
  lcd.setCursor(0, 1);
  lcd.print(sixteenSpaces);
  lcd.setCursor(0, 1);
  lcd.print(text);
}

void clearLCD() {
  lcd.setCursor(0, 0);
  lcd.print(sixteenSpaces);
  lcd.setCursor(0, 1);
  lcd.print(sixteenSpaces);
  lcd.setCursor(0, 0);
}

void resetData() {
  for (int i = 0; i < LANES; i++) {
    previousStates[i] = HIGH;
    for (int j = 0; j < MAX_TIMES; j++) {
      times[i][j] = 0;
    }
    debounceData[i] = 0;
    presses[i] = 0;
  }
  resetState = HIGH;
  resetDebounce = 0;
  timing = false;
  Serial.println("Ready");
  printLCD("Ready");
}

void sendData() {
  printLCD("Uploading...");
  for (int i = 0; i < LANES; i++) {
    Serial.print(i);
    Serial.print(": ");
    for (int j = 0; j < presses[i]; j++) {
      Serial.print(times[i][j]);
      Serial.print(",");
    }
    Serial.println("");
  }  
}

unsigned long debounceButton(int pin, int& previousState, unsigned long& previousTime) {
  unsigned long rc = 0;
  unsigned long now = millis();
  int state = digitalRead(pin);
  if (state == previousState) {
    return rc;
  }
  if (state == LOW) {
    if (previousTime == 0) {
      previousTime = now;
    }
  } else {
    if (now - previousState > DEBOUNCE_DELAY) {
      rc = previousTime;
      previousTime = 0;
    }
  }
  previousState = state;
  return rc;
}

void checkForMessage() {
  if (Serial.available() == 0) {
    return;
  }

  int i = 0;
  while (Serial.available() > 0) {
    int c = (char)Serial.read();
    // Anything more than 16 characters we ignore
    if (i < 16) {
      buf[i++] = (char)c;
    }
  }
  printLCD1(buf);  
}

void loop() {
  // See if our controller is sending us a message
  checkForMessage();

  // Check for reset/upload
  if (debounceButton(RESET, resetState, resetDebounce) > 0) {
    sendData();
    resetData();
  } else {
    for (int i = 0; i < LANES; i++) {
      unsigned long pressed = debounceButton(IN[i], previousStates[i], debounceData[i]);

      if (pressed > 0) {
        if (timing == false) {
          timing = true;
          printLCD1("Started");
        }
        sprintf(buf, "%lu", pressed);
        printLCD2(buf);
      }
      if (pressed > 0 && presses[i] < MAX_TIMES) {
        times[i][presses[i]] = pressed;
        presses[i]++;
      }
    }
  }
}
